#ifndef _CWPHOTO
#define _CWPHOTO

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <gphoto2/gphoto2.h>
#include <gphoto2/gphoto2-camera.h>

#define CAMERA "Canon EOS 60D"

class CWPhoto
{
	public:
		CWPhoto();
		~CWPhoto();
		void shoot( char *name );

	private:
		Camera *camera;
		GPContext *context;		

		int setModel( const char *model );
		void capture_to_file( Camera *camera, GPContext *context, char *fn );
};


#endif