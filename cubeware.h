#ifndef _CUBEWARE
#define _CUBEWARE

#include "cwconfig.h"
#include "cwproto.h"
#include "cwphoto.h" 
#include "txn_super.h"
#include "arduino.h"
#include "detector.h"

#define NUM_CUBES 5
#define ARDPORT "/dev/ttyACM0"

class CubeWare
{
	public:
		CubeWare() {
			iter_calc = 0;
			table_calc = 0;
			status = STATUS_NOPLAY;				
			time_left = 0;
			request = 0;				
		};

		void init();
		void kick();

	private:
		int iter_calc;
		int table_calc;
		int status;	
		string game_id;
		int time_left;
		int request;	

		CWConfig cconfig;
		CWProto cproto;
		CWPhoto cphoto;
		Arduino arduino;
		Detector detect;
};


#endif