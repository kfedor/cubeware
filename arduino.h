#ifndef _ARDUINO
#define _ARDUINO

#include "serialdev.h"

#define JSSPEED B115200

#define CMDLEN 5
#define REPLEN 5

#define STX 0x05
#define ETX 0x06

#define CMD_TABLE 1
#define CMD_MOVIE 2
#define CMD_BUTTON 3

class Arduino : public SerialDev 
{
	public:
		Arduino() {
			cmdid = 0;
			protoLenPos = 0;
			protoLen = 0;
			protoRepLen = REPLEN;
			enableParity = 0;
			baudRate = JSSPEED;
		};

		enum btnstat {
			error = 0x00,
			on = 0x01,
			off = 0x02
		};
		
		int table( char );
		int movie( char );
		btnstat readButton();
 		
	private:
		unsigned char cmdid;
		unsigned char genID() { return ++cmdid; }
		int checkRes( unsigned char *cmd, int );
};

#endif