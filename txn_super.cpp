#include "txn_super.h"

txn_super::txn_super()
{
	th_net = 0;
	ready = 0;
}

txn_super::txn_super( string host, string body )
{
	th_net = 0;
	ready = 0;
	trans_host = host;
	trans_body = body;
}

txn_super::~txn_super()
{  
	if( th_net )
		pthread_join( th_net, NULL );
}

void txn_super::sendThread()
{
	pthread_create( &th_net, NULL, threadSendHandler, this );
}

void *txn_super::threadSendHandler( void *txn )
{
	txn_super *sup = (txn_super*)txn;

	sup->send();
	
	//g_idle_add_full ( G_PRIORITY_DEFAULT, txn_super_report, (gpointer)txn, NULL );
}

void txn_super::print()
{
	cout << trans_body;	
}

size_t txn_super::curlHandle( void *ptr, size_t size, size_t nmemb, void *stream )
{  	
	txn_super *sup = (txn_super*)stream;
	
	sup->reply.assign( (char*)ptr, nmemb*size );
	
	return size*nmemb;		
}

int txn_super::send()
{
	CURL *curl;
	CURLcode res;
	struct curl_slist *headerlist=NULL;
	const char buf[] = "Content-type: application/json";

	success = 0;
	if( trans_body.length() == 0 )
	{
		printf("No transaction body\n");		
		return success;
	}

	curl_global_init( CURL_GLOBAL_ALL );

	curl = curl_easy_init();

	headerlist = curl_slist_append( headerlist, buf );	

	if( !curl )
		return success;
	
	curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, curlHandle );
	curl_easy_setopt( curl, CURLOPT_WRITEDATA, this );
	curl_easy_setopt( curl, CURLOPT_URL, trans_host.c_str() );
	curl_easy_setopt( curl, CURLOPT_HTTPHEADER, headerlist );		

	curl_easy_setopt( curl, CURLOPT_POST, 1 );
	curl_easy_setopt( curl, CURLOPT_POSTFIELDS, trans_body.c_str() );

	//curl_easy_setopt( curl, CURLOPT_CONNECTTIMEOUT, 5 ); // 30 sec
	//curl_easy_setopt( curl, CURLOPT_NOSIGNAL, 0 ); // wah wah
	//curl_easy_setopt( curl, CURLOPT_VERBOSE, 1 );
	
	res = curl_easy_perform( curl );

	curl_easy_cleanup( curl );
	curl_slist_free_all( headerlist );

	if( res != CURLE_OK )
	{
		printf( "curl_easy_perform() failed: %s\n", curl_easy_strerror(res) );
		return success;
	}

	success = 1;
	            
	return success;
}