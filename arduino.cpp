#include "arduino.h"

int Arduino::checkRes( unsigned char *cmd, int )
{
	if( cmd[0] == STX && cmd[4] == ETX && cmd[3] == cmdid )		
		return 1;

	printf("Arduino::checkRes fail\n");
	return 0;
}

int Arduino::table( char on )
{
	unsigned const char cmd[CMDLEN] = { STX, CMD_TABLE, on, genID(), ETX };

	if( !sendCmd( cmd, CMDLEN ) )
		return 0;	

	return 1;
}


int Arduino::movie( char on )
{
	unsigned const char cmd[CMDLEN] = { STX, CMD_MOVIE, on, genID(), ETX };

	if( !sendCmd( cmd, CMDLEN ) )
		return 0;	

	return 1;
}

Arduino::btnstat Arduino::readButton()
{
	unsigned const char cmd[CMDLEN] = { STX, CMD_BUTTON, 0, genID(), ETX };

	if( !sendCmd( cmd, CMDLEN ) )
		return error;	

	if( reply[2] == 1 )
		return on;
	else if( reply[2] == 0 )
		return off;
	
	return error;
}