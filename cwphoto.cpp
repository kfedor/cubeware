#include "cwphoto.h"

CWPhoto::CWPhoto()
{
	int index = 0;	
	
	context = gp_context_new();
	gp_camera_new( &camera );

	//setModel( CAMERA );

	printf("Camera init\n");
	gp_camera_init( camera, context );
}

CWPhoto::~CWPhoto()
{
	gp_camera_exit( camera, context );
	gp_context_unref( context );
}

int CWPhoto::setModel( const char *model )
{
	int ret;
	CameraAbilities a;
	CameraAbilitiesList *list;

//	gp_camera_get_abilities( camera, &a );
	
	ret = gp_abilities_list_new( &list );
	ret = gp_abilities_list_load( list, context );
	printf("We have %d abilities\n", gp_abilities_list_count( list ) );
	
	int m = gp_abilities_list_lookup_model( list, model );	
	if( m )
	{
		ret = gp_abilities_list_get_abilities( list, m, &a );
		printf("[%d]: Model [%s]\n", m, a.model );	
		ret = gp_camera_set_abilities( camera, a );		

		gp_setting_set ("gphoto2", "model", a.model);

		return 1;
	}
	
	return 0;
}


void CWPhoto::shoot( char *name )
{
	capture_to_file( camera, context, name );
}

void CWPhoto::capture_to_file( Camera *camera, GPContext *context, char *fn ) 
{
	int fd, retval;
	CameraFile *file;
	CameraFilePath camera_file_path;

	printf("Capturing.\n");

	/* NOP: This gets overridden in the library to /capt0000.jpg */
	strcpy( camera_file_path.folder, "/" );
	strcpy( camera_file_path.name, "foo.jpg" );

	retval = gp_camera_capture( camera, GP_CAPTURE_IMAGE, &camera_file_path, context );
	
	printf("Pathname on the camera: %s/%s\n", camera_file_path.folder, camera_file_path.name);

	fd = open(fn, O_CREAT | O_WRONLY, 0644);
	retval = gp_file_new_from_fd(&file, fd);
	
	retval = gp_camera_file_get(camera, camera_file_path.folder, camera_file_path.name, GP_FILE_TYPE_NORMAL, file, context);

	printf("Deleting.\n");
	
	retval = gp_camera_file_delete(camera, camera_file_path.folder, camera_file_path.name,
			context);	

	gp_file_free(file);
}
