CC = c++
LIBS = -ljsoncpp -lcurl -lpthread -lcrypto -lgphoto2 -lzbar -lev `pkg-config --libs --cflags Magick++`
INC = -I /usr/include/jsoncpp
SOURCES = main.cpp cwconfig.cpp cubeware.cpp cwproto.cpp txn_super.cpp cwphoto.cpp arduino.cpp fserialport.cpp serialdev.cpp detector.cpp
OUTNAME = CubeWare
GPHOTO = -L /usr/lib/i386-linux-gnu/

all:
	$(CC) $(SOURCES) -o $(OUTNAME) $(INC) $(LIBS) $(GPHOTO) 

clean:
	rm *.o *.*~

