#ifndef _DETECTOR
#define _DETECTOR

#define STEP 20
#define NTHR 100/STEP

#include <ev.h>
#include <iostream>
#include <set>
#include <Magick++.h>
#include <zbar.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include <vector>
#include <string>

#define DETECT_COMPLETE 1
#define DETECT_PROCESS  2
#define DETECT_IDLE     3

using namespace std;
using namespace zbar;

class Detector
{
	public:		
		int initImage( char *image );
		int detect( int numImages );

	private:
		int detected;
		//int 
		
};

#endif