#ifndef _SDEV
#define _SDEV

#include <string>
#include "fserialport.h"

using namespace std;

#define ACK 0x06
#define NAK 0x15

class SerialDev
{
	public:
		virtual ~SerialDev() {
			serial.close();
		};
		
		int init( string port );
		int sendLine( const unsigned char *cmd, int length );
		int sendCmd( const unsigned char *cmd, int length );
		int sendCmdAck( const unsigned char *cmd, int length );

	protected:
		int replen;
		unsigned char reply[255];

		int protoLenPos;
		int protoLen;
		int protoRepLen;
		int enableParity;
		int baudRate;
		
	private:			
		FSerialPort serial;

		void printHex( unsigned char *data, int i );
		virtual int checkRes( unsigned char *cmd, int len ) = 0;		
};

#endif