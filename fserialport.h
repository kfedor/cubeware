#ifndef _SPORT
#define _SPORT

#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <fcntl.h>
#include <unistd.h>

#include <termios.h>
#include <sys/ioctl.h>
#include <string.h>

class FSerialPort 
{
	public:		
		FSerialPort() {
			pdesc = 0;	
		};

		void enableParityBit();
		bool isOpen();

		int open( const char *, int );		
		int write( const char *data, int len );
		int read( unsigned char *buf, int bufsize );
		void close();
		int selectRead( unsigned char *buf, int bufsize );
		int sendRecv( char *command, int csize, unsigned char *buf, int bufsize );
		int getFD() { return pdesc; };
		void flush() { tcflush( pdesc, TCSANOW ); };

	private:
		int pdesc;
		void setDTR();
		void setRTS();
};

#endif
