#ifndef _CWPROTO
#define _CWPROTO

#include <string>
#include <set>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <openssl/md5.h>
#include <unistd.h>

#include "json/value.h"
#include "json/writer.h"
#include "json/reader.h"

#define API_VERSION "1.0"
#define MD5_DIGEST_LENGTH 16

#define STATUS_NOPLAY  0  // розыгрыш не назначен
#define STATUS_PLAY    1  // розыгрыш назначен
#define STATUS_PLAYING 3  // розыгрыш в процессе 
#define STATUS_OFF     10 // стол выключен 
#define STATUS_NOTFULL 2
#define STATUS_FULL    5
#define STATUS_FAIL    6  // игра не состоялась и зафейлилась

#define COMMAND_OK     1
#define COMMAND_PLAY   2

#define ERROR_NO 0
#define ERROR_DATA 1
#define ERROR_AUTH 2

using namespace std;

class CWProto
{
	public:
		string& getRequest( string key, string game_id, int status, int time_left, set<string> game_result );
		int setReply( string reply );
		int getCommand() { return r_command; }
		int getTime() { return r_time; }
		string& getGame() { return r_gameid; }

	private:
		Json::Value root;
		string proto;

		int r_error;
		int r_command;		
		int r_time;
		string r_gameid;
		
		int getTimestamp();
		int generateRequest( string key, string game_id, int status, int time_left, set<string> game_result );
		int generateKey( string &, int, string );
};

#endif