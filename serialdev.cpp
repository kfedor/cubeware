#include "serialdev.h"

void SerialDev::printHex( unsigned char *data, int i )
{
	int q;

	printf("[%d]: ", i); 
	
	for( q=0; q<i; q++ )
		printf("%02X ", (unsigned char)data[q]);

	printf("\n");
}

int SerialDev::init( string port )
{	
	if( !serial.open( port.c_str(), baudRate ) )
	{
		printf("SerialDev: cannot open serial port [%s]\n", port.c_str() );
		return 0;
	}

	if( enableParity )
		serial.enableParityBit();
	
	return 1;
}

int SerialDev::sendLine( const unsigned char *cmd, int length )
{
	printHex( (unsigned char*)cmd, length );
	
	if( !serial.write( (const char*)cmd, length ) )
	{
		printf("SerialDev: write failed\n");
		return 0;
	}

	return 1;
}

int SerialDev::sendCmdAck( const unsigned char *cmd, int length )
{
	char ret;
	int cmdlen = 0;	
	int res;			
	
	printf("<===== ");
	printHex( (unsigned char*)cmd, length );
	
	if( !serial.write( (const char*)cmd, length ) )
	{
		printf("SerialDev: write failed\n");
		return 0;
	}

	res = 0;
	memset( reply, 0, sizeof(reply) );	

	res = serial.read( (unsigned char*)&ret, 1 ); 
	if( !res )
	{
		printf("SerialDev: read failed\n");
		return 0;
	}
	
	if( ret == ACK )
	{
		printf("SerialDev::ACK\n");

		replen = 0;
		for( int i=0; i<=20; i++ )
		{
			res = serial.read( reply+replen, sizeof(reply)-replen ); 
			if( res )
			{				
				replen += res;
				cmdlen = protoRepLen;
				if( !cmdlen && replen >= protoLenPos )								
					cmdlen = protoLen + (int)reply[ protoLenPos-1 ];
				
				// >= instead of == added for WST002 variable length replies
				if( cmdlen && replen >= cmdlen )				
					break;				
			}
		}
	} 
	else if( ret == NAK )
	{
		printf("SerialDev::NAK\n");
		return 0;
	}

	printHex( reply, replen );
	
	if( !checkRes( reply, replen ) )
		return 0;		
	
	return 1;
}

int SerialDev::sendCmd( const unsigned char *cmd, int length )
{
	int iterMax;
	int res;	
	int cmdlen = 0;
	
	printf("<===== ");
	printHex( (unsigned char*)cmd, length );
	
	if( !serial.write( (const char*)cmd, length ) )
	{
		printf("SerialDev: write failed\n");
		return 0;
	}

	res = 0;
	memset( reply, 0, sizeof(reply) );

	replen = 0;

	if( protoLenPos )
		iterMax = 20;
	else
		iterMax = 2;
	
	for( int i=0; i<=iterMax; i++ )
	{
		res = serial.read( reply+replen, sizeof(reply)-replen ); 
		if( res )
		{				
			replen += res;			
			
			cmdlen = protoRepLen;
			if( !cmdlen && replen >= protoLenPos )				
					cmdlen = protoLen + (int)reply[ protoLenPos-1 ];								
			
			// >= instead of == added for WST002 variable length replies			
			if( cmdlen && replen >= cmdlen )	
				break;
		}
	}

	printf("=====> ");
	printHex( reply, replen );

	if( !checkRes( reply, replen ) )
		return 0;			
	
	return 1;
}
