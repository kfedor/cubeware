#ifndef _SUPER
#define _SUPER

#include <string>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <pthread.h>

#include <curl/curl.h>

using namespace std;

class txn_super
{
	public:
		txn_super();
		txn_super( string host, string body );
		~txn_super();

			
		void print();		
		int send();
		void sendThread();		
		int isReady() { return ready; }
		int isSuccess() { return success; }
		
		string reply;						
		
	private:	
		int ready;
		int success;
		string trans_body;
		string trans_host;

		pthread_t th_net;
		
		static void *threadSendHandler( void *txn );
		static size_t curlHandle( void *ptr, size_t size, size_t nmemb, void *stream );	
};


#endif