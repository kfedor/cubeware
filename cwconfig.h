#ifndef _CWCONFIG
#define _CWCONFIG

#include <string>
#include <iostream>
#include <fstream>

#include "json/value.h"
#include "json/writer.h"
#include "json/reader.h"

#define CONFIGFILE "./config.json"

using namespace std; 

class CWConfig
{
	public:		
		int restore();
		string& getKey() { return key; }
		string& getHost() { return host; }
		int getAckTime() { return ack_time; }
		int getPort() { return port; }
		int getTableRunTime() { return table_run_time; } 
		
	private:
		int port;
		int ack_time;
		int table_run_time;
		
		string host;
		string key;

		Json::Reader reader;
		Json::Value root;
};



#endif