#include "cwproto.h"

int CWProto::setReply( string data )
{
	Json::Reader reader;
	Json::Value reply;

	printf("Reply[%s]\n", data.c_str() );
	
	if( !reader.parse( data, reply ) )
		return 0;

	r_error = reply.get( "error", -1 ).asInt();

	switch( r_error )
	{
		case -1:
		case ERROR_DATA:
		case ERROR_AUTH:
			printf("CWProto::setReply global error\n");
			return 0;			
	}

	string tmpcmd = reply.get( "command", "0" ).asString();
	if( tmpcmd.compare( "0" ) == 0 )
		return 0;

	if( tmpcmd.compare( "ok" ) == 0 )
	{
		r_command = COMMAND_OK;
	}
	else if( tmpcmd.compare( "play" ) == 0 )
	{
		r_command = COMMAND_PLAY;

		r_time = reply["data"].get( "time", -1 ).asInt();
		r_gameid = reply["data"].get( "id", "0" ).asString();
	}
	
	return 1;
}

void md5it( unsigned char *outbuffer, const unsigned char *input )
{
	MD5_CTX ctx;
	MD5_Init(&ctx);
	MD5_Update(&ctx, input, (unsigned int)strlen((char *)input));
	MD5_Final(outbuffer, &ctx);
}

string& CWProto::getRequest( string key, string game_id, int status, int time_left, set<string> game_result )
{
	Json::FastWriter writer;

	generateRequest( key, game_id, status, time_left, game_result );
	proto = writer.write( root );

	return proto;
}

int CWProto::generateRequest( string key, string game_id, int status, int time_left, set<string> game_result )
{
	int id = getTimestamp();
	string newKey;
	generateKey( newKey, id, key );

	root.clear();
	root["request_id"] = id;
	root["key"] = newKey;
	root["version"] = API_VERSION;
	root["request_timestamp"] = getTimestamp();
	root["data"]["status"] = status;

	switch( status )
	{
		case STATUS_NOPLAY:
		case STATUS_PLAYING:
		case STATUS_OFF:
			root["command"] = "status";
			break;
			
		case STATUS_PLAY:	
			root["command"] = "status";
			root["data"]["id"] = game_id;	
			root["data"]["time_left"] = time_left;
			break;

		case STATUS_FAIL:	
			root["command"] = "result";
			root["data"]["id"] = game_id;
			break;	

		case STATUS_NOTFULL:
		case STATUS_FULL:
			root["command"] = "result";
			root["data"]["id"] = game_id;

			set<string>::iterator it;
			for( it = game_result.begin(); it != game_result.end(); ++it ) {
				root["data"]["result"].append( *it ); 
			}

			break;
	};
}

int CWProto::getTimestamp()
{
	time_t res = time (NULL);
	return res;	
}


int CWProto::generateKey( string &fin, int id, string key )
{
	unsigned char catted[255] = {0};
	unsigned char md5buf[MD5_DIGEST_LENGTH] = {0};
	unsigned char result[255] = {0};

	snprintf( (char*)catted, sizeof(catted), "%s%d", key.c_str(), id );	
	
	md5it( (unsigned char*)md5buf, (const unsigned char*)catted );
	
	for( int i=0; i<MD5_DIGEST_LENGTH; i++ )	
		snprintf( (char *)&result[i*2], 3, "%02x", md5buf[i] );	      
	
	fin.assign( (char*)result );
	
	return 1;	
}