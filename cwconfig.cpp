#include "cwconfig.h"

int CWConfig::restore()
{
	ifstream in;
	root.clear();

	in.open( CONFIGFILE );
	if( !in.is_open() )
	{
		printf("Error opening [%s]\n", CONFIGFILE );
		return 0;
	}
	
	if( !reader.parse( in, root ) )
	{
		printf("Error parsing [%s]\n", CONFIGFILE );
		in.close();
		return 0;
	}

	in.close();

	host = root["server"].get( "host", "0" ).asString();
	key = root["server"].get( "key", "0" ).asString();
	port = root["server"].get( "port", 0 ).asInt();
	
	ack_time = root["timers"].get( "ack_time", 0 ).asInt();
	table_run_time = root["timers"].get( "table_run_time", 0 ).asInt();

	if( !ack_time || !table_run_time || !port )
		return 0;
	
	return 1;
}