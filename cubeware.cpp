#include "cubeware.h"

// globals
extern set<string> game_result;
extern int detector_status;

void CubeWare::init()
{
	cconfig.restore();	
	arduino.init( ARDPORT );
	sleep( 2 ); // Arduino requires this
	arduino.table( 0 );
	arduino.movie( 0 );
}

void CubeWare::kick()
{
	// Check button
	Arduino::btnstat btn = arduino.readButton();

	printf("Status = [%d]\n", status );

	switch( status )
	{			
		case STATUS_NOPLAY:
			if( btn == Arduino::off ) 
				status = STATUS_OFF;								

			break;

		case STATUS_OFF:
			if( btn == Arduino::on ) {
				printf("Table switched ON\n");
				status = STATUS_NOPLAY;
			}
			else {
				printf("Table off\n");
			}
			break;

		case STATUS_PLAY:
			if( btn == Arduino::off ) {
				status = STATUS_OFF;
				break;
			}

			if( time_left > 0 )	{				
				printf("Play scheduled in %d secs, game_id [%s]\n", time_left, game_id.c_str() );
				time_left--;
			} else {
				printf("Staring play now, game_id [%s]\n", game_id.c_str() );
				status = STATUS_PLAYING;

				table_calc = 0;
				// RUN GAME NOW
				arduino.table( 1 );
				arduino.movie( 1 );
			}
			break;

		case STATUS_PLAYING:
			if( btn == Arduino::off ) { // какой то пидор выключил стол				
				status = STATUS_FAIL; // game fail
				break;
			}

			table_calc++;
			printf("Table calc [%d]\n", table_calc );

			if( table_calc >= cconfig.getTableRunTime() ) {
				// Game finished
				printf("Play finished\n");					

				arduino.table( 0 );
				arduino.movie( 0 );

				// run camera
			
				printf("Detector status [%d]\n", detector_status );
				
				// check status					
				if( detector_status == DETECT_COMPLETE )
				{
					detector_status = DETECT_IDLE; 
					
					if( game_result.size() == NUM_CUBES )
						status = STATUS_FULL;
					else
						status = STATUS_NOTFULL;
				} 
				else if( detector_status != DETECT_PROCESS )
				{
					printf("Run detector!\n");
					      
					detector_status = DETECT_PROCESS;
					detect.initImage( "aaa.jpg" );
					detect.detect( NUM_CUBES );
				}	
			}
			break;

		case STATUS_NOTFULL:
		case STATUS_FULL:
		case STATUS_FAIL:
			status = STATUS_NOPLAY;
			break;
	};

	// network request 
	if( ( status == STATUS_FULL ) || ( status == STATUS_NOTFULL ) || ( status == STATUS_FAIL ) || ( iter_calc == cconfig.getAckTime() ) )
	{
		iter_calc = 0;

		string prt = cproto.getRequest( cconfig.getKey(), game_id, status, time_left, game_result );
		string host = cconfig.getHost(); 

		txn_super csuper( host, prt );

		csuper.print();

		if( csuper.send() )
		{
			if( cproto.setReply( csuper.reply ) )
			{
				int cmd = cproto.getCommand();
				printf("Command [%d]\n", cmd );

				if( cmd == COMMAND_PLAY )
				{									
					status = STATUS_PLAY;
					time_left = cproto.getTime();
					game_id = cproto.getGame();
					printf("Play command received, game id [%s]\n", game_id.c_str() );
				}
			}
		}
	}
	// request end

	iter_calc++;
}