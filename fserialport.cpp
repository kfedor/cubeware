#include "fserialport.h"

bool FSerialPort::isOpen()
{
	if( pdesc ) 
		return 1;
}
//0:0:8bd:0:0:0:0:0:0:e8:0:0:0:0:0:0:1:0:0:8:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0
//35b819a7:0:3f9d4cfd:b5ab2ff4:34:ab:b5:0:0:32:0:38:8:0:0:dc:6d:98:b5:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0

int FSerialPort::open( const char *name, int baudRate ) 
{
	struct termios tc;

	memset( &tc, 0, sizeof(struct termios) );

	//O_RDWR or O_NONBLOCK or O_NOCTTY moschip 
	pdesc = ::open( name, O_RDWR ); //| O_NONBLOCK | O_NOCTTY );
	//pdesc = ::open( name, O_RDWR | O_NONBLOCK | O_NOCTTY );
	
	if( pdesc < 0 ) 
		return 0;

	tcdrain( pdesc );
	tcflush( pdesc, TCIOFLUSH );
	
	cfsetspeed( &tc, baudRate );

	tc.c_cflag |= CS8 | CLOCAL | CREAD; 	
	tc.c_cflag &= ~CRTSCTS;

	//raw binary input / output
	tc.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
	tc.c_oflag &= ~OPOST;

    //no flow control
	tc.c_iflag = 0;
	tc.c_iflag &= ~(IXON | IXOFF | IXANY);	
	
	tc.c_cc[VMIN] = 0;       // threshold = 0 characters 
	tc.c_cc[VTIME]  = 50;    // timeout: 100 = 10 sec
	
	if( tcsetattr( pdesc, TCSAFLUSH, &tc ) < 0 ) 
	{
		::close( pdesc );
		return -1;
	}
	
	return 1;
}

void FSerialPort::setDTR()
{
	int status;
	status &= ~TIOCM_DTR;
	ioctl( pdesc, TIOCMSET, &status );
}

void FSerialPort::setRTS()
{
	int status;
	status &= ~TIOCM_RTS;
	ioctl( pdesc, TIOCMSET, &status );	
}

// PARENB added for Sankyo ICT
void FSerialPort::enableParityBit()
{
	struct termios tc;
 	memset( &tc, 0, sizeof(struct termios) );

	tcgetattr( pdesc, &tc );
	
	tc.c_cflag |= PARENB;

	if( tcsetattr( pdesc, TCSAFLUSH, &tc ) < 0 ) 
		fprintf( stderr, "FSerialPort:: enabling parity bit failed\n");
}

void FSerialPort::close()
{
	tcflush( pdesc, TCIOFLUSH );	
	::close( pdesc );
}


int FSerialPort::write( const char *data, int len )
{	
	while( len > 0 ) 
	{		
		int wr = ::write( pdesc, data, len );

		if ( wr < 0 ) 
		{
			perror("FSerialPort::write error");
			return 0;
		}

		if (wr == 0) 
		{
			fprintf(stderr, "FSerialPort::write cannot send data\n");
			return 0;
		}

		data += wr;
		len -= wr;
	}

	return 1;
}

int FSerialPort::read( unsigned char *buf, int bufsize )
{
	int ret;
	
	if( !pdesc )
		return 0;

	ret = ::read( pdesc, (unsigned char*)buf, bufsize );		
	
	return ret;
}

int FSerialPort::selectRead( unsigned char *buf, int bufsize )
{
	int res;
	fd_set readfs;
	struct timeval tm;
	int r;

	FD_ZERO ( &readfs );
	FD_SET ( pdesc, &readfs );
	tm.tv_sec = 5; // sec
	tm.tv_usec = 0; // millisec

	do
	{
		r = select ( pdesc + 1, &readfs, NULL, NULL, &tm );
	}
	while( r == 0 && !(tm.tv_sec == 0 && tm.tv_usec == 0) );

	if( r <= 0 )
	{
		perror("Select failed");
		return 0;
	}
		
	res = ::read( pdesc, buf, bufsize );
	
	if( res < 0 ) 
	{
		perror("Read failed");
		return 0;
	}
	
	return res;
}

int FSerialPort::sendRecv( char *command, int csize, unsigned char *buf, int bufsize )
{
	int res;
	fd_set readfs;
	struct timeval tm;
	int r;
	
	if ( !write( command, csize ) )
	{
		return 0;
	}

	FD_ZERO ( &readfs );
	FD_SET ( pdesc, &readfs );
	tm.tv_sec = 5;
	tm.tv_usec = 0;

	do
	{
		r = select ( pdesc + 1, &readfs, NULL, NULL, &tm );
	}
	while( r == 0 && !(tm.tv_sec == 0 && tm.tv_usec == 0) );

	if( r <= 0 )
	{
		return 0;
	}
		
	res = read( buf, bufsize );
	
	if( res < 0 ) 
	{
		return 0;
	}
	
	return res;
}

