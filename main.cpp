#include "cubeware.h"

CubeWare cube;

// for ev
pthread_mutex_t mymutex = PTHREAD_MUTEX_INITIALIZER;
struct ev_loop *loop = EV_DEFAULT;  //or ev_default_loop (0);
ev_async async_watcher;
ev_timer timer_watcher;

// globals
set<string> game_result;
int detector_status;

static void async_cb( EV_P_ ev_async *w, int revents )
{
	printf( "async cb: game_result size = %d\n", game_result.size() );

	if( game_result.size() == NUM_CUBES )
		detector_status = DETECT_COMPLETE;
}

static void timer_cb( EV_P_ struct ev_timer* w, int revents )
{
	cube.kick();
}

int main()
{
	cube.init();
	
	ev_async_init( &async_watcher, async_cb );
	ev_async_start( loop, &async_watcher );
	ev_timer_init( &timer_watcher, timer_cb, 0, 1 );
	ev_timer_start( loop, &timer_watcher);
 
	ev_loop( loop, 0 );
	
	return 1;
}
